# -*- coding: utf-8 -*-
import serial
import simplejson

class Monitor(object):
    def __init__(self):
        self._con = serial.Serial("/dev/ttyACM0", 9600)
        self._measure_co2 = [0]
        self._gas = [0]
        self._humedity = [0]
        self._temperature = [0]
        self._light_uv = [0]
        self._moisture = [0]
        self._t = [0]
        self._t_init = 0

    def read_sensors(self):
        jsonResult = self._con.readline()
        try:
            jsonObject = simplejson.loads(jsonResult)
            self._measure_co2.append(jsonObject["measure_co2"])
            self._gas.append(jsonObject["gas"])
            self._humedity.append(jsonObject["humedity"])
            self._temperature.append(jsonObject["temperature"])
            self._light_uv.append(jsonObject["light_uv"])
            self._moisture.append(jsonObject["moisture"])
            self._t_init += 1
            self._t.append(self._t_init)
        except Exception:
            pass

    def decode_info(self): #,info
        info_decoded = ""
        return info_decoded #decoded

    def getT(self):
        return self._t

    def getGas(self):
        return self._gas

    def getCO2(self):
        return self._measure_co2

    def getHumedity(self):
        return self._humedity

    def getTemperature(self):
        return self._temperature

    def getLightUV(self):
        return self._light_uv

    def getSoilMoisture(self):
        return self._moisture
