# -*- coding: utf-8 -*-
from PyQt5 import QtCore, QtGui, QtWidgets

import pyqtgraph as pg
from dialog_ui import Ui_Dialog

import sys
sys.path.append("..")
from control.monitor_controller import Monitor

class Ui_MainWindow(object):

    def closeEvent(self):
        app.quit()

    def gasEvent(self):
        self.graph = 0

    def co2Event(self):
        self.graph = 1

    def hum_tempEvent(self):
        self.graph = 2

    def moistureEvent(self):
        self.graph = 3

    def lightEvent(self):
        self.graph = 4

    def infoEvent(self):
        title = "Informacion"
        info = "Informacion"
        dialog = QtWidgets.QDialog()
        ui_dialog = Ui_Dialog()
        ui_dialog.setupUi(dialog)
        ui_dialog.setInfo(title, info)
        dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        dialog.exec_()

    def aboutEvent(self):
        title = "Acerca de"
        info = "Informacion"
        dialog = QtWidgets.QDialog()
        ui_dialog = Ui_Dialog()
        ui_dialog.setupUi(dialog)
        ui_dialog.setInfo(title, info)
        dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        dialog.exec_()

    def helpEvent(self):
        title = "Ayuda"
        info = "Informacion"
        dialog = QtWidgets.QDialog()
        ui_dialog = Ui_Dialog()
        ui_dialog.setupUi(dialog)
        ui_dialog.setInfo(title, info)
        dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        dialog.exec_()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(887, 600)
        self.graph = 2
        self.monitor = Monitor()
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.exit_button = QtWidgets.QPushButton(self.centralwidget)
        self.exit_button.setGeometry(QtCore.QRect(720, 490, 131, 41))
        self.exit_button.setObjectName("exit_button")
        self.title = QtWidgets.QLabel(self.centralwidget)
        self.title.setGeometry(QtCore.QRect(20, 40, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.title.setFont(font)
        self.title.setObjectName("title")
        self.description = QtWidgets.QLabel(self.centralwidget)
        self.description.setGeometry(QtCore.QRect(43, 88, 191, 391))
        self.description.setWordWrap(True)
        self.description.setObjectName("description")
        self.sensor_1 = QtWidgets.QLabel(self.centralwidget)
        self.sensor_1.setGeometry(QtCore.QRect(230, 10, 111, 51))
        self.sensor_1.setObjectName("sensor_1")
        self.sensor_2 = QtWidgets.QLabel(self.centralwidget)
        self.sensor_2.setGeometry(QtCore.QRect(360, 10, 111, 51))
        self.sensor_2.setObjectName("sensor_2")
        self.sensor_3 = QtWidgets.QLabel(self.centralwidget)
        self.sensor_3.setGeometry(QtCore.QRect(490, 10, 111, 51))
        self.sensor_3.setObjectName("sensor_3")
        self.sensor_4 = QtWidgets.QLabel(self.centralwidget)
        self.sensor_4.setGeometry(QtCore.QRect(620, 10, 111, 51))
        self.sensor_4.setObjectName("sensor_4")
        self.sensor_5 = QtWidgets.QLabel(self.centralwidget)
        self.sensor_5.setGeometry(QtCore.QRect(750, 10, 111, 51))
        self.sensor_5.setObjectName("sensor_5")
        self.type_sensor = QtWidgets.QLabel(self.centralwidget)
        self.type_sensor.setGeometry(QtCore.QRect(20, 80, 231, 19))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.type_sensor.setFont(font)
        self.type_sensor.setObjectName("type_sensor")
        self.gridLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(270, 80, 581, 401))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.graph_view = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.graph_view.setContentsMargins(1, 1, 1, 1)
        self.graph_view.setObjectName("graph_view")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 887, 27))
        self.menubar.setObjectName("menubar")
        self.menuSensores = QtWidgets.QMenu(self.menubar)
        self.menuSensores.setObjectName("menuSensores")
        self.menuInformaci_n = QtWidgets.QMenu(self.menubar)
        self.menuInformaci_n.setObjectName("menuInformaci_n")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionSensor_DHT11 = QtWidgets.QAction(MainWindow)
        self.actionSensor_DHT11.setObjectName("actionSensor_DHT11")
        self.actionSensor_DHT11.triggered.connect(self.hum_tempEvent)
        self.actionGUVA_P1918 = QtWidgets.QAction(MainWindow)
        self.actionGUVA_P1918.setObjectName("actionGUVA_P1918")
        self.actionGUVA_P1918.triggered.connect(self.lightEvent)
        self.actionMQ_135 = QtWidgets.QAction(MainWindow)
        self.actionMQ_135.setObjectName("actionMQ_135")
        self.actionMQ_135.triggered.connect(self.gasEvent)
        self.actionMIKROE_1630 = QtWidgets.QAction(MainWindow)
        self.actionMIKROE_1630.setObjectName("actionMIKROE_1630")
        self.actionMIKROE_1630.triggered.connect(self.co2Event)
        self.actionDFROBOT_Sensor = QtWidgets.QAction(MainWindow)
        self.actionDFROBOT_Sensor.setObjectName("actionDFROBOT_Sensor")
        self.actionDFROBOT_Sensor.triggered.connect(self.moistureEvent)
        self.actionInformacion = QtWidgets.QAction(MainWindow)
        self.actionInformacion.setObjectName("actionInformacion")
        self.actionInformacion.triggered.connect(self.infoEvent)
        self.actionAcerca_de = QtWidgets.QAction(MainWindow)
        self.actionAcerca_de.setObjectName("actionAcerca_de")
        self.actionAcerca_de.triggered.connect(self.aboutEvent)
        self.actionAyuda = QtWidgets.QAction(MainWindow)
        self.actionAyuda.setObjectName("actionAyuda")
        self.actionAyuda.triggered.connect(self.helpEvent)
        self.menuSensores.addAction(self.actionMQ_135)
        self.menuSensores.addAction(self.actionMIKROE_1630)
        self.menuSensores.addSeparator()
        self.menuSensores.addAction(self.actionSensor_DHT11)
        self.menuSensores.addAction(self.actionDFROBOT_Sensor)
        self.menuSensores.addSeparator()
        self.menuSensores.addAction(self.actionGUVA_P1918)
        self.menuInformaci_n.addAction(self.actionAyuda)
        self.menuInformaci_n.addAction(self.actionInformacion)
        self.menuInformaci_n.addSeparator()
        self.menuInformaci_n.addAction(self.actionAcerca_de)
        self.menubar.addAction(self.menuSensores.menuAction())
        self.menubar.addAction(self.menuInformaci_n.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.graphWidget = pg.PlotWidget()
        self.graph_view.addWidget(self.graphWidget, 0, 0)
        self.exit_button.clicked.connect(self.closeEvent)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Redes III - Monitor de Sensores"))
        self.exit_button.setText(_translate("MainWindow", "Salir"))
        self.title.setText(_translate("MainWindow", "Información"))
        self.description.setText(_translate("MainWindow", "Lorem ipsum "))
        self.sensor_1.setText(_translate("MainWindow", "Sensor 1"))
        self.sensor_2.setText(_translate("MainWindow", "Sensor 2"))
        self.sensor_3.setText(_translate("MainWindow", "Sensor 3"))
        self.sensor_4.setText(_translate("MainWindow", "Sensor 4"))
        self.sensor_5.setText(_translate("MainWindow", "Sensor 5"))
        self.type_sensor.setText(_translate("MainWindow", "TextLabel"))
        self.menuSensores.setTitle(_translate("MainWindow", "Sensores"))
        self.menuInformaci_n.setTitle(_translate("MainWindow", "Ayuda"))
        self.actionSensor_DHT11.setText(_translate("MainWindow", "Sensor DHT11 "))
        self.actionGUVA_P1918.setText(_translate("MainWindow", "GUVA P1918"))
        self.actionMQ_135.setText(_translate("MainWindow", "MQ - 135"))
        self.actionMIKROE_1630.setText(_translate("MainWindow", "MIKROE-1630"))
        self.actionDFROBOT_Sensor.setText(_translate("MainWindow", "DFROBOT Sensor"))
        self.actionInformacion.setText(_translate("MainWindow", "Informacion"))
        self.actionAcerca_de.setText(_translate("MainWindow", "Acerca de"))
        self.actionAyuda.setText(_translate("MainWindow", "Ayuda"))

    def update_sensors(self):
        g = self.monitor.getGas()[-1]
        self.sensor_1.setText("Gas:\n"+str(g))
        c = self.monitor.getCO2()[-1]
        self.sensor_2.setText("CO2:\n"+str(c))
        h = self.monitor.getHumedity()[-1]
        t = self.monitor.getTemperature()[-1]
        self.sensor_3.setText("Hum:"+str(h)+"\nTemp:"+str(t))
        h_s = self.monitor.getSoilMoisture()[-1]
        self.sensor_4.setText("Hum. Suelo:\n"+str(h_s))
        l = self.monitor.getLightUV()[-1]
        self.sensor_5.setText("Luz:\n"+str(l))

    def update(self):
        self.monitor.read_sensors()
        self.update_sensors()
        x = self.monitor.getT()
        if self.graph == 0:
            graph = True
            title = "Sensor de Gas"
            type = "MQ - 135"
            description = " "
            y = self.monitor.getGas()
        elif self.graph == 1:
            graph = True
            title = "Sensor de CO2"
            type = "MIKROE 1630"
            description = " "
            y = self.monitor.getCO2()
        elif self.graph == 2:
            graph = False
            title = "DHT11"
            type = "Humedad Temperatura"
            description = " "
            y = self.monitor.getHumedity()
            y_t = self.monitor.getTemperature()
            line_sty_aux = pg.mkPen(color = (255, 51, 51), width = 3)
            self.graphWidget.plot(x, y_t, pen = line_sty_aux, clear = True)
        elif self.graph == 3:
            graph = True
            title = "Humedad de Suelo"
            type = "DFROBOT"
            description = " "
            y = self.monitor.getSoilMoisture()
        elif self.graph == 4:
            graph = True
            title = "Sensor de Luz"
            type = "GUVA P1918"
            description = " "
            y = self.monitor.getLightUV()
        self.title.setText(title)
        self.type_sensor.setText(type)
        self.description.setText(description)
        line_sty = pg.mkPen(color = (51, 51, 255), width = 3)
        self.graphWidget.plot(x, y, pen = line_sty, clear = graph)
        QtCore.QTimer.singleShot(1, self.update)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    ui.update()
    sys.exit(app.exec_())
